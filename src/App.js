import React, { Component } from "react";
import { YMaps, Map, RouteEditor } from "react-yandex-maps";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import "./App.css";

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};
const getItemStyle = (isDragging, draggableStyle) => ({
  background: isDragging ? "lightgreen" : "grey",
  ...draggableStyle
});
const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : "lightgrey",
  padding: 10
});

class App extends Component {
  state = { width: "100%", height: 500, points: [] };
  onApiAvaliable(ymaps) {
    ymaps
      .route(this.state.points, {
        mapStateAutoApply: true
      })
      .then(route => {
        route.getPaths().options.set({
          balloonContentBodyLayout: ymaps.templateLayoutFactory.createClass(
            "$[properties.humanJamsTime]"
          ),
          strokeColor: "0000ffff",
          opacity: 0.9
        });
        route.editor.start();
        this.map.geoObjects.add(route);
      });
  }
  onDragEnd(result) {
    if (!result.destination) {
      return;
    }
    const points = reorder(
      this.state.points,
      result.source.index,
      result.destination.index
    );
    this.setState({ points });
  }
  addCoords(e) {
    if (e.key === "Enter") {
      let points = [...this.state.points];
      points.push(e.target.value);
      e.target.value = "";
      this.setState({ points });
    }
  }
  deletePoint(index) {
    const { points } = this.state;
    points.splice(index, 1);
    this.setState({ points });
  }
  render() {
    const mapState = { center: [55.76, 37.64], zoom: 10 };
    const { width, height, points } = this.state;
    const MyMap = () => (
      <YMaps onApiAvaliable={ymaps => this.onApiAvaliable(ymaps)}>
        <Map
          state={mapState}
          width={width}
          height={height}
          instanceRef={ref => (this.map = ref)}
        >
          <RouteEditor />
        </Map>
      </YMaps>
    );
    const Points = () => {
      return points.map((item, index) => {
        return (
          <Draggable key={index} draggableId={index} index={index}>
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={getItemStyle(
                  snapshot.isDragging,
                  provided.draggableProps.style
                )}
                className="point"
              >
                <div className="point__name">{item}</div>
                <div
                  className="point__del"
                  onClick={() => {
                    this.deletePoint(index);
                  }}
                >
                  x
                </div>
              </div>
            )}
          </Draggable>
        );
      });
    };
    return (
      <div className="App">
        <div className="container">
          <div className="d-flex">
            <div className="points">
              <div>
                <input
                  type="text"
                  className="points__input"
                  onKeyPress={e => {
                    this.addCoords(e);
                  }}
                />
                <div className="points__list">
                  <DragDropContext onDragEnd={this.onDragEnd.bind(this)}>
                    <Droppable droppableId="droppable">
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          style={getListStyle(snapshot.isDraggingOver)}
                        >
                          <Points />
                          {provided.placeholder}
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                </div>
              </div>
            </div>
            <div className="map-container">
              <MyMap />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
